## Next Starter kennymark

A front-end for an api that includes all the necessary pages and components for a complete user management application

## Pages
- Home
- Login
- Sign up
- Dashboard
- Team
- Account Page
- Email Confirmation
- Password 

## Tech stack

- Chakra-ui
- Next.js
- React hook form
- Chakra-ui
- Easy Peasy
- React query for Optimistic ui

## Features

> Has UI for all the following

- Authentication 
- Authorization 
- Password Reset flow
- Toast notifications 
- Optimistic UI with React Query
- Update account
- Update password
- Delete account




