import { Box, Flex, Icon } from '@chakra-ui/react'
import { MenuAlt1Icon } from '@heroicons/react/outline'
import useAuth from '@hooks/useAuth'
import React, { Fragment, useState } from 'react'
import MenuItems from './header/menu'
import NavLink from './nav-link'
import RenderIf from './render-if'

const Header = () => {
  const { isLoggedIn } = useAuth()
  const [show, setShow] = useState(false)
  const handleToggle = () => setShow(!show)

  return (
    <Flex
      as='nav'
      alignItems={['start', 'center']}
      px={10}
      py={6}
      direction={['column', 'row']}
      color='gray.700'>
      <Box display={['inline', 'none']} mr={3} onClick={handleToggle} color='gray.800'>
        <Icon as={MenuAlt1Icon} fontSize='2xl' mb={4} />
      </Box>

      <Flex flexGrow={1} direction={['column', 'row']} display={[show ? 'flex' : 'none', 'flex']}>
        <NavLink to='/' mr='2'>
          Home
        </NavLink>
        <NavLink to='/dashboard'>Dashboard</NavLink>
      </Flex>

      <Flex
        alignItems={['start', 'center']}
        direction={['column', 'row']}
        display={[show ? 'flex' : 'none', 'flex']}>
        <RenderIf condition={!isLoggedIn} elseShow={<MenuItems />}>
          <NavLink to='/signin' mr={2}>
            Signin
          </NavLink>
          <NavLink to='/signup'>Register</NavLink>
        </RenderIf>
      </Flex>
    </Flex>
  )
}

export default React.memo(Header)
