import { Link as ChakraLink, LinkProps } from '@chakra-ui/react'
import Link from 'next/link'
import React from 'react'

interface NavLinkProps extends LinkProps {
  children?: string | React.ReactNode
  to: string
  activeProps?: LinkProps
}

function NextLink({ to, activeProps, children, ...props }: NavLinkProps) {
  return (
    <Link href={to}>
      <ChakraLink {...props} _hover={{ color: 'blue.600' }}>
        {children}
      </ChakraLink>
    </Link>
  )
}

export default NextLink
