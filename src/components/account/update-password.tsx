import { Button, chakra, Heading, useToast } from '@chakra-ui/react'
import { isButtonEnabled } from '@shared/utils/form-utils'
import PasswordInput from '@shared/components/password-input'
import userService from '@services/user.service'
import { updatePasswordSchema } from '@shared/utils/schemas'
import { toastOptions } from '@shared/utils/toastOptions'
import { AxiosError } from 'axios'
import { Form, FormikProvider, useFormik } from 'formik'
import React from 'react'
import { useMutation } from 'react-query'

function UpdatePassword({ email }) {
  const toast = useToast()

  const formik = useFormik({
    validateOnChange: true,
    validationSchema: updatePasswordSchema,
    initialValues: { password: '', new_password: '', password_confirmation: '' },
    onSubmit: (values) => {
      const newData = { email, ...values }
      mutate(newData)
    },
  })

  const { mutate } = useMutation(userService.updatePassword, {
    onSuccess: ({ data }) => {
      toast({ description: data?.message, ...toastOptions(3, 'success') })
      formik.resetForm()
    },
    onError: ({ response: { data } }: AxiosError) => {
      toast({ description: data?.message, ...toastOptions(3, 'error') })
    },
  })

  return (
    <>
      <Heading fontSize='lg' mt={20}>
        Update Password
      </Heading>

      <FormikProvider value={formik}>
        <chakra.form as={Form} mt={5} autoComplete='off'>
          <PasswordInput label='Current Password' id='username' name='password' />
          <PasswordInput label='New Password' id='new_pass' name='new_password' />
          <PasswordInput label='Confirm Password' id='pass_confirm' name='password_confirmation' />

          <Button variant='main' w={60} type='submit' isDisabled={isButtonEnabled(formik)}>
            Update Password
          </Button>
        </chakra.form>
      </FormikProvider>
    </>
  )
}

export default UpdatePassword
