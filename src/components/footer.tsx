import { Center, Flex } from "@chakra-ui/react";


function Footer() {

  return (
    <Flex borderTop='1px' borderTopColor='gray.200' justifyContent='center' alignItems='center' height={100}>
      <Center>Copyright 2020</Center>
    </Flex>
  );

}

export default Footer;
