import { Text, useToast } from '@chakra-ui/react'
import useAuth from '@hooks/useAuth'
import authService from '@services/auth.service'
import { toastOptions } from '@shared/utils/toastOptions'
import { AxiosError } from 'axios'
import React from 'react'
import { useMutation } from 'react-query'

function Signout() {
  const toast = useToast()
  const { signout } = useAuth()

  const { mutate } = useMutation(authService.signOut, {
    onMutate: () => {
      signout()
    },
    onSuccess: async ({ data }) => {
      toast({ description: data.message, ...toastOptions(2, 'success') })
    },
    onError: async ({ response: { data } }: AxiosError) => {
      toast({ description: data.message, ...toastOptions(2, 'error') })
    },
  })

  return (
    <Text
      onClick={() => mutate()}
      p={3}
      fontSize='sm'
      color='lightBlack'
      _hover={{
        bg: 'blue.50',
        rounded: 'md',
        cursor: 'pointer',
        color: 'alt',
        fontWeight: 'bold',
      }}>
      Signout
    </Text>
  )
}

export default Signout
