import {
  Avatar,
  Button,
  chakra,
  Popover,
  PopoverBody,
  PopoverContent,
  PopoverTrigger,
  Text,
} from '@chakra-ui/react'
import NavLink from '@components/nav-link'
import useAuth from '@hooks/useAuth'
import React from 'react'
import Signout from './signout'

const MenuItemLink = ({ children, to }) => (
  <NavLink
    to={to}
    color='lightBlack'
    cursor='pointer'
    p={3}
    _hover={{ bg: 'blue.50', rounded: 'md', color: 'alt', fontWeight: 'semibold' }}
    fontWeight='normal'
    fontSize='sm'>
    {children}
  </NavLink>
)

function MenuItems() {
  const { user } = useAuth()

  return (
    <Popover placement='bottom-start' trigger='hover'>
      <PopoverTrigger>
        <Button _focus={{}} _active={{}} variant='ghost' px={0} _hover={{ color: 'blue.400' }}>
          <chakra.span pr={2}>{user?.name}</chakra.span>
          <Avatar size='sm' bg='blue.100' name={user?.name} color='alt' />
        </Button>
      </PopoverTrigger>

      <PopoverContent _focus={{}} rounded='xl' shadow='lg' maxW={300}>
        <PopoverBody display='flex' flexDir='column' p={3}>
          <MenuItemLink to='/account'>
            <Text fontWeight='semibold'>{user?.name}</Text>
            <Text fontSize='xs' color='gray.500'>
              View Profile
            </Text>
          </MenuItemLink>

          <MenuItemLink to='/team'>Team</MenuItemLink>
          <Signout />
        </PopoverBody>
      </PopoverContent>
    </Popover>
  )
}

export default MenuItems
