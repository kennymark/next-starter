import { Button } from '@chakra-ui/button'
import { Center, Stack, Text } from '@chakra-ui/react'
import useLocalStorage from '@hooks/useStorage'
import React from 'react'

function AllowCookies() {
  const [value, setAllowcookies] = useLocalStorage('allow-cookies')

  const accept = () => {
    setAllowcookies(true)
  }

  const deny = () => {
    setAllowcookies(false)
  }

  return (
    <>
      {!Boolean(value) && (
        <Stack pos='fixed' bottom={2} width='100%' bg='white'>
          <Center
            w='90%'
            mx='auto'
            border='1px'
            p={4}
            rounded='lg'
            borderColor='gray.200'
            shadow='lg'>
            <Stack>
              <Text>
                We use cookies to make your experience on this site better. To comply with
                regulations, we asked for your consent to set cookies
              </Text>

              <Center display={{ base: 'block', md: 'flex' }}>
                <Button onClick={accept} mr={2} variant='main' w={{ base: '100%', md: 60 }}>
                  Accept
                </Button>
                <Button onClick={deny} mr={2} variant='secondary' w={{ base: '100%', md: 60 }}>
                  Deny
                </Button>
              </Center>
            </Stack>
          </Center>
        </Stack>
      )}
    </>
  )
}

export default AllowCookies
