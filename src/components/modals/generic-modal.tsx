import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogCloseButton,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  AlertDialogProps,
  Button,
  chakra,
  UseDisclosureProps,
} from '@chakra-ui/react'
import React from 'react'

export interface BaseModalProps extends UseDisclosureProps {
  onYesClick: (...args: any) => void
  onNoClick?: (...args: any) => void
  yesText?: string
  noText?: string
  title?: string
  description?: string
  children?: React.ReactNode
  size?: AlertDialogProps['size']
}

function Modal({
  isOpen,
  onClose,
  onYesClick,
  onNoClick,
  children,
  yesText = 'Continue',
  noText = 'Cancel',
  title,
  size,
  description,
}: BaseModalProps) {
  const cancelRef = React.useRef()

  return (
    <>
      <AlertDialog
        motionPreset='slideInBottom'
        leastDestructiveRef={cancelRef}
        size={size}
        onClose={onClose}
        isOpen={isOpen}
        isCentered>
        <AlertDialogOverlay />

        <AlertDialogContent>
          <AlertDialogHeader>{title}</AlertDialogHeader>
          <AlertDialogCloseButton />
          <AlertDialogBody>
            <chakra.div mb={3}>{description}</chakra.div>

            {children}
          </AlertDialogBody>

          <AlertDialogFooter>
            <Button ref={cancelRef} onClick={onYesClick} variant='main'>
              {yesText}
            </Button>
            <Button colorScheme='red' ml={3} onClick={onNoClick || onClose} variant='secondary'>
              {noText}
            </Button>
          </AlertDialogFooter>
        </AlertDialogContent>
      </AlertDialog>
    </>
  )
}

export default Modal
