import { Button, Tag, useDisclosure, useToast } from '@chakra-ui/react'
import messages from '@shared/data/toast-messages'
import userService from '@services/user.service'
import { toastOptions } from '@shared/utils/toastOptions'
import { AxiosError } from 'axios'
import React from 'react'
import { useMutation } from 'react-query'
import Modal from './generic-modal'

function DeleteUserModal({ id }) {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const toast = useToast()

  const { mutate } = useMutation(userService.deleteUser, {
    onSuccess: ({ data: res }) => {
      toast({ description: res?.message, ...toastOptions(3, 'success') })
      onClose()
    },

    onError: ({ response: { data } }: AxiosError) => {
      toast({ ...messages.defaultError, ...toastOptions(3, 'error') })
    },
  })

  // TODO: Redirect after login to signIn Page
  const handleDeleteAccount = () => mutate(id)

  return (
    <>
      <Button p={6} variant='danger' onClick={onOpen} mt={5}>
        Delete Account
      </Button>

      <Modal
        isOpen={isOpen}
        onClose={onClose}
        onYesClick={handleDeleteAccount}
        onNoClick={onClose}
        title='Delete user'
        yesText='Delete'>
        Are you sure you want to your account? Your account and data will be deleted.{' '}
        <Tag colorScheme='red'>Not implemented yet</Tag>
      </Modal>
    </>
  )
}

export default DeleteUserModal
