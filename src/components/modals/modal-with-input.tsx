import { Input } from '@chakra-ui/react'
import { useFormik } from 'formik'
import React from 'react'
import Modal, { BaseModalProps } from './generic-modal'

interface Props extends BaseModalProps {
  placeholder: string
  inputName: string
}

function ModalWithInput({ inputName, onYesClick, placeholder, ...props }: Props) {
  const submitRef = React.useRef<HTMLInputElement>()

  const { handleSubmit, getFieldProps } = useFormik({
    validateOnChange: true,
    initialValues: { [inputName]: '' },
    onSubmit: (values) => onYesClick(values),
  })

  return (
    <Modal {...props} onYesClick={() => submitRef.current?.click()}>
      <form onSubmit={handleSubmit}>
        <Input variant='filled' placeholder={placeholder} my={4} {...getFieldProps(inputName)} />
        <input type='submit' hidden ref={submitRef} />
      </form>

      {props.children}
    </Modal>
  )
}

export default ModalWithInput
