import { Text, useToast } from '@chakra-ui/react'
import useAuth from '@hooks/useAuth'
import authService from '@services/auth.service'
import { useStoreActions } from '@store/hooks'
import { toastOptions } from '@shared/utils/toastOptions'
import { useRouter } from 'next/router'
import React from 'react'
import { useMutation, useQueryClient } from 'react-query'
import Modal from './generic-modal'

function SessionExpiredModal() {
  const toast = useToast()
  const router = useRouter()
  const { signout, user, setAuth, isSessionModalOpen } = useAuth()
  const { toggleSessionModal } = useStoreActions((state) => state.auth)
  const qc = useQueryClient()

  const { mutate } = useMutation(authService.reauthenticate, {
    onSuccess: async ({ data }) => {
      setAuth({ user, token: data.token, expires_at: '' })
      toast({ ...toastOptions(2, 'success'), description: data.message })
      await qc.cancelQueries()
      await qc.refetchQueries()
    },
    onError: async () => {
      toast({ ...toastOptions(3, 'error'), description: 'Error...' })
      await qc.refetchQueries()
    },
    onMutate: () => {
      toggleSessionModal(false)
      router.replace(router.asPath)
    },
  })

  return (
    <Modal
      isOpen={isSessionModalOpen}
      onYesClick={() => mutate(user)}
      onNoClick={() => {
        toggleSessionModal(false)
        signout()
      }}
      onClose={() => {
        toggleSessionModal(false)
      }}
      title='Session Expired'
      yesText='Reauthenticate'
      noText='Logout'>
      <Text>
        Your session as expired, to proceed using the application, Please re-authenticate your
        session to continue
      </Text>
    </Modal>
  )
}

export default SessionExpiredModal
