import { Container, Flex } from '@chakra-ui/react'
import useProgress from '@hooks/useProgress'
import { useStoreActions } from '@store/hooks'
import Router from 'next/router'
import { useEffect } from 'react'
import Footer from './footer'
import Header from './nav'
import SEO from './seo'

interface Props {
  sm?: boolean
  children?: React.ReactNode
  title?: string
}

function Layout({ children, sm, title = 'Home' }: Props) {
  const { setCurrentUrl } = useStoreActions((state) => state.url)
  const { startProgress, stopProgress } = useProgress()

  useEffect(() => {
    Router.events.on('routeChangeStart', (_) => startProgress())
    Router.events.on('routeChangeComplete', (_) => stopProgress())
    Router.events.on('routeChangeError', (_) => stopProgress())

    return () => {
      Router.events.off('routeChangeStart', (_) => stopProgress())
      Router.events.off('routeChangeComplete', (_) => stopProgress())
      Router.events.off('routeChangeError', (_) => stopProgress())
    }
  }, [])

  useEffect(() => {
    Router.events.on('routeChangeComplete', setCurrentUrl)
  }, [])

  return (
    <Flex direction='column'>
      <SEO title={title} />
      <Header />
      <Container p={4} minH='80vh' maxW={sm ? '2xl' : '4xl'}>
        {children}
      </Container>
      <Footer />
    </Flex>
  )
}

export default Layout
