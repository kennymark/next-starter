import { Button, Heading, Link, Stack, Text } from '@chakra-ui/react'

function CallToActionWithAnnotation() {
  return (
    <Stack
      textAlign={'center'}
      spacing={{ base: 8, md: 14 }}
      py={{ base: 20, md: 36 }}
      w='fit-content'>
      <Heading fontWeight={600} fontSize={{ base: '2xl', sm: '4xl', md: '6xl' }}>
        Chakra UI/ Next.js Starter
        <br />
        <Text as={'span'} color={'blue.400'}>
          with backend
        </Text>
      </Heading>

      <Text color={'gray.500'}>
        This template includes Formik, Chakra-ui, Easy-peasy(a wrapper around redux), React-query
        for data fetching and Yup for validation. The backend is built with Adonis.js
      </Text>

      <Stack spacing={3} alignSelf={'center'}>
        <Button variant='secondary' mt={0} px={6}>
          Get Started
        </Button>
        <Link color='blue.600' href='https://kennymark.com/'>
          @mrkennymark
        </Link>
      </Stack>
    </Stack>
  )
}

export default CallToActionWithAnnotation
