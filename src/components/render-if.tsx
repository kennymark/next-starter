import React from 'react'

function RenderIf({ condition, children, elseShow }) {
  if (condition) {
    return <>{children}</>
  }

  return <>{elseShow}</>
}

export default RenderIf
