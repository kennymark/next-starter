import React from 'react'
import { ArrowBackIcon } from '@chakra-ui/icons'
import { useRouter } from 'next/router'
import { Tooltip } from '@chakra-ui/react'

function BackArrow({ to = '/' }) {
  const router = useRouter()

  const goBack = () => {
    router.push(to)
  }

  return (
    <Tooltip hasArrow label='Go home' rounded='lg'>
      <ArrowBackIcon onClick={goBack} boxSize={6} />
    </Tooltip>
  )
}

export default BackArrow
