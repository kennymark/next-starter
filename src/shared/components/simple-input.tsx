import { FormControl, FormLabel, Input, InputProps } from '@chakra-ui/react'
import React from 'react'
import { capitalize, startCase } from 'lodash'

interface SimpleInput extends InputProps {
  label?: string
}
function SimpleInput({ name, label, ...props }: SimpleInput) {
  return (
    <FormControl mb={3}>
      <FormLabel htmlFor={name}>{label || capitalize(startCase(name))}</FormLabel>
      <Input id={name} name={name} {...props} />
    </FormControl>
  )
}

export default SimpleInput
