import { FormControl, FormLabel, forwardRef, Input, InputProps, Text } from '@chakra-ui/react'
import { useField } from 'formik'
import { capitalize, startCase } from 'lodash'
import React from 'react'

type Ref = React.LegacyRef<HTMLInputElement>

export interface TextInputProps extends InputProps {
  label?: string
}

const TextInput = forwardRef(({ label, ...props }: TextInputProps, ref: Ref) => {
  const [field, meta] = useField(props as any)
  const [didFocus, setDidFocus] = React.useState(false)

  const handleFocus = () => setDidFocus(true)
  const showFeedback = (!!didFocus && field.value.length > 0) || meta.touched

  return (
    <FormControl mb={3}>
      <FormLabel htmlFor={props.id || props.name}>{startCase(props.name) || label}</FormLabel>
      <Input
        {...props}
        {...field}
        id={props.id || props.name}
        _focus={{ borderColor: meta.error ? 'red.600' : 'green.600' }}
        aria-describedby={`${props.id}-feedback ${props.id}-help`}
        onFocus={handleFocus}
        ref={ref}
      />
      {showFeedback ? (
        <Text aria-live='polite' color={meta.error ? 'red.400' : 'green.600'} pt={1} fontSize='xs'>
          {capitalize(meta.error ? meta.error : 'Correct ✓').replace(/\_|\//, ' ')}
        </Text>
      ) : null}
    </FormControl>
  )
})

export default TextInput
