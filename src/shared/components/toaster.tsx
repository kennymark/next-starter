import { CloseIcon } from '@chakra-ui/icons'
import { chakra, useToast, Text, Flex, Icon } from '@chakra-ui/react'
import {
  BadgeCheckIcon,
  ExclamationCircleIcon,
  ExclamationIcon,
  InformationCircleIcon,
} from '@heroicons/react/solid'
import { useEffect, useState } from 'react'

interface SnackbarProps {
  title?: string
  description?: string
  duration?: number
  variant?: 'success' | 'error' | 'warning' | null
}

/**
 *
 * @param duration - Should be a single digit represent seconds
 * @returns an invocable function
 */
function useCustomToast(props = {} as SnackbarProps): (props: SnackbarProps) => void {
  const data = {
    variant: '',
    duration: 3,
    title: '',
    description: '',
  }
  const [id] = useState(Date.now())

  useEffect(() => {}, [id])

  const color =
    (props.variant == 'warning' && 'yellow.200') ||
    (props.variant == 'error' && 'red.200') ||
    (props.variant == 'success' && 'green.200') ||
    (!props.variant && 'blue.200')

  const icon =
    (props.variant == 'warning' && ExclamationIcon) ||
    (props.variant == 'error' && ExclamationCircleIcon) ||
    (props.variant == 'success' && BadgeCheckIcon) ||
    (!props.variant && InformationCircleIcon)

  const display = ({ variant, duration = 3, title, description }: SnackbarProps) => {
    data.variant = variant
    data.duration = duration
    data.description = description
    data.title = title
    toast()
  }

  const toast = useToast({
    id,
    isClosable: true,
    duration: data.duration * 1000 || props.duration * 1000,
    render: () => (
      <chakra.div
        p={4}
        bg='white'
        rounded='lg'
        minH={100}
        border='1px'
        borderColor='gray.200'
        minW={350}
        maxW={400}
        shadow='md'>
        <Flex justify='space-between' pb={2} alignItems='center'>
          <chakra.div display='flex' alignItems='center'>
            <Icon mr={2} as={icon} color={color} fontSize='xl' />
            <Text fontWeight='bold'>{data.title || props.title}</Text>
          </chakra.div>
          <Icon as={CloseIcon} onClick={() => toast.close(id)} cursor='pointer' fontSize='sm' />
        </Flex>

        <Text fontSize='md'>{data.description || props.description}</Text>
      </chakra.div>
    ),
  })

  return display
}

export default useCustomToast
