import { ViewIcon, ViewOffIcon } from '@chakra-ui/icons'
import { chakra, InputGroup, InputRightElement } from '@chakra-ui/react'
import React from 'react'
import TextInput, { TextInputProps } from './text-input'

const PasswordInput = (props: TextInputProps) => {
  const [show, setShow] = React.useState(false)
  const handleClick = () => setShow(!show)

  return (
    <InputGroup>
      <TextInput
        id={props.id || `password`}
        name={props.name || 'password'}
        type={show ? 'text' : 'password'}
        autoComplete='off'
        {...props}
      />
      <InputRightElement width='4rem'>
        <chakra.div onClick={handleClick} cursor='pointer' pos='relative' top='30px'>
          {!show ? <ViewOffIcon boxSize={5} /> : <ViewIcon boxSize={5} />}
        </chakra.div>
      </InputRightElement>
    </InputGroup>
  )
}

export default PasswordInput
