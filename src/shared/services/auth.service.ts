import { ResetPassword, User } from '../models/user.model'
import { api } from './api.service'

const baseUrl = 'account/'

type PUser = Partial<User>

class AuthService {
  test() {
    return api.get('/')
  }

  signIn(user: PUser) {
    return api.post(baseUrl + 'signin', user)
  }

  reauthenticate(user: PUser) {
    return api.post(baseUrl + 'reauthenticate', user)
  }

  signUp(user: PUser) {
    return api.post(baseUrl + 'signup', user)
  }

  sendConfirmationEmail(email: string, signature: string) {
    return api.post(baseUrl + 'confirmation/' + email + `?signature=${signature}`)
  }

  resendConfirmationEmail(values: string) {
    return api.post(baseUrl + 'resend-confirmation/' + values)
  }

  forgotPassword(values: { email: string }) {
    return api.post(baseUrl + 'forgot-password', values)
  }

  resetPassword(url: string, passwords: ResetPassword) {
    return api.post(baseUrl.replace(/\//, '') + url, { ...passwords })
  }

  signOut() {
    return api.post(baseUrl + 'signout')
  }
}

export default new AuthService()
