import { User } from '../models/user.model'
import { api } from './api.service'

const baseUrl = 'account/user'

class UserService {
  updateUser(user: User) {
    return api.put(baseUrl, user)
  }

  updatePassword(user: {
    password: string
    new_password: string
    password_confirmation: string
    email: string
  }) {
    return api.put(baseUrl + '/update-password', user)
  }

  deleteUser(id: number | string) {
    return api.delete(baseUrl + `/${id}`)
  }

  getTeam() {
    return api.get('account/team')
  }
}

export default new UserService()
