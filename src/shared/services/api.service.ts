import axios from 'axios'
import Cookies from 'js-cookie'

const URLS = {
  development: 'http://localhost:3333/api/',
  production: 'https://adonis-starter-production.up.railway.app/api/',
}

const token = {
  getToken(): string {
    return Cookies.get('token')
  },

  setToken(token: string) {
    Cookies.set('token', token)
  },
  remove() {
    Cookies.remove('token')
  },
}

const api = axios.create({
  // baseURL: URLS['production'],
  baseURL: URLS[process.env.NODE_ENV],
  headers: {
    authorization: `Bearer ${token.getToken()}`,
  },
})

export { token, api }
