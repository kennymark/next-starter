import { api } from './api.service'

class OauthService {
  async sendGithubCode(code: string) {
    return api.get('oauth/github', { params: { code } })
  }

  async fetchTwitterToken() {
    return api.get('oauth/twitter')
  }

  async sendTokenAndVerifier(query) {
    return api.post('oauth/twitter', query)
  }
}

export default new OauthService()
