import { toastOptions } from './toastOptions'
import { capitalize } from 'lodash'

function serverErrors(error: any): any[] {
  return error?.response.data?.errors ?? []
}

function serverErrorToast<T extends Function, Err>(toast: T, err: Err, title: string) {
  serverErrors(err).forEach(({ message }) =>
    toast({ title, description: capitalize(message), status: 'error', ...toastOptions(1) })
  )
}

export { serverErrors, serverErrorToast }
