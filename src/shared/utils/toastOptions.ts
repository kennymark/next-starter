import { AlertStatus, UseToastOptions } from '@chakra-ui/react'

/**
 *
 * @param duration time in seconds like 1
 * @returns
 */

function toastOptions(dur = 5, status?: AlertStatus): UseToastOptions {
  return {
    position: 'top',
    isClosable: true,
    variant: 'subtle',
    title: status == 'error' ? 'Error' : status === 'success' && 'Success',
    duration: dur * 1000,
    status,
  }
}

export { toastOptions }
