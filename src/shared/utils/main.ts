type Service = 'github' | 'google' | 'twitter'

const isDev = process.env.NODE_ENV === 'development'

export function filterError(value: string) {
  return value.replace('E_AUTHORIZATION_FAILURE: ', '')
}

export function getRedirectUrl(service: Service) {
  if (isDev) {
    return 'https://localhost:3000/oauth/' + service
  }
  return 'https://next-adonis.vercel.app/oauth/' + service
}

export { isDev }
