import { createStore, persist } from 'easy-peasy'
import AuthStore, { AuthStoreModel } from './auth'
import UrlStore, { UrlStoreModel } from './url'

const store = createStore(
  {
    auth: persist(AuthStore, { storage: 'localStorage' }),
    url: UrlStore,
  },
  {
    devTools: true,
    name: 'next-starter',
  }
)

export interface storeModel {
  auth: AuthStoreModel
  url: UrlStoreModel
}

export default store
