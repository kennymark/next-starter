import { api, token } from '@services/api.service'
import { action, Action, ActionOn, actionOn, Computed, computed } from 'easy-peasy'
import { User } from '../models/user.model'
import store from '.'
import router from 'next/router'

const authStore: AuthStoreModel = {
  data: { user: null, expires_at: '', token: '' },
  isLoggedIn: computed((state) => !!state.data?.user),
  user: computed((state) => state.data?.user),

  isSessionModalOpen: false,

  setAuth: action((state, payload) => {
    state.data = payload
  }),

  signout: action((state) => {
    state.data = null
    store.persist.clear().then((_) => {
      token.remove()
      router.push('/')
    })
  }),

  toggleSessionModal: action((state, payload) => {
    state.isSessionModalOpen = payload
  }),

  setUser: action((state, payload) => {
    state.data.user = { ...state.data.user, ...payload }
  }),

  onSignIn: actionOn(
    (actions) => actions.setAuth,
    (state, target) => {
      state.user = target.payload.user
      token.setToken(target.payload.token)
      api.defaults.headers.authorization = `Bearer ${token.getToken()}`
      if (!state.user) {
        location.assign('/signin')
      }
    }
  ),

  onSignOut: actionOn(
    (actions) => actions.signout,
    (_) => {}
  ),
}

export interface AuthStoreModel {
  data: { user: User; expires_at: string; token: string; updated_at?: string }
  user?: Computed<AuthStoreModel, User>
  isSessionModalOpen: boolean
  signout?: Action<AuthStoreModel, void | string>
  isLoggedIn: Computed<AuthStoreModel, boolean>
  setAuth: Action<AuthStoreModel, AuthStoreModel['data']>
  setUser: Action<AuthStoreModel, User>
  toggleSessionModal: Action<AuthStoreModel, boolean>
  onSignIn?: ActionOn<AuthStoreModel>
  onSignOut?: ActionOn<AuthStoreModel>
}

export default authStore
