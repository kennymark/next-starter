// hooks.js
import { createTypedHooks } from 'easy-peasy'
import { storeModel } from './index'

const { useStoreActions, useStoreState, useStore } = createTypedHooks<storeModel>()

export { useStoreActions, useStoreState, useStore }
