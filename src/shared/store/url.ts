import { Action, action } from 'easy-peasy'

const UrlStore: UrlStoreModel = {
  currentURL: '',

  setCurrentUrl: action((state, payload) => {
    if (!payload.includes('signin')) {
      state.currentURL = payload
    }
  }),
}

export interface UrlStoreModel {
  currentURL: string
  setCurrentUrl: Action<UrlStoreModel, string>
}

export default UrlStore
