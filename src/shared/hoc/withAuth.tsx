import { useToast } from '@chakra-ui/react'
import messages from '@shared/data/toast-messages'
import useAuth from '@hooks/useAuth'
import { User } from '@shared/models/user.model'
import { api } from '@services/api.service'
import { useStoreActions } from '@store/hooks'
import { toastOptions } from '@shared/utils/toastOptions'
import {useRouter} from 'next/router'
import React, { useEffect } from 'react'

interface withAuthProps {
  data: { user: User; expires_at: string; token: string }
}

const withAuth = <P extends withAuthProps | any>(Component: React.ComponentType<P>) => (
  props: P
) => {
  const toast = useToast({ id: 'session' })
  const { isLoggedIn, data } = useAuth()
  const errCode = 401
  const { toggleSessionModal } = useStoreActions((state) => state?.auth)
  const router = useRouter()
  let interceptor: number

  useEffect(() => {
    // checkAuthenticatedStatus()

    if (!isLoggedIn) {
      router.replace('/signin')
      toast({ ...messages.notLoggedIn, ...toastOptions(1, 'warning') })
    }

    checkForUnauthorizedAccess()
    
    return () => {
      api.interceptors.response.eject(interceptor)
    }
  }, [])


  function checkForUnauthorizedAccess() {
    interceptor = api.interceptors.response.use(
      (res) => res,
      (error) => {
        if (error?.response?.status === errCode) {
          toggleSessionModal(true)
          !toast.isActive('session') && toast({ ...messages.sessionExpired, ...toastOptions(1) })
        }
        return Promise.reject(error)
      }
    )
  }
  

  return <Component {...props} {...data} />
}

export default withAuth
