import { useToast } from '@chakra-ui/react'
import messages from '@shared/data/toast-messages'
import useAuth from '@hooks/useAuth'
import { useStoreState } from '@store/hooks'
import { toastOptions } from '@shared/utils/toastOptions'
import router from 'next/router'
import React, { useEffect } from 'react'

const withGuest = <P extends object>(Component: React.ComponentType<P>) => (props: P) => {
  const toast = useToast()
  // const { currentURL } = useStoreState((state) => state.url)
  const { isLoggedIn } = useAuth()

  useEffect(() => {
    checkAuthenticatedStatus()
  }, [])

  async function checkAuthenticatedStatus() {
    if (isLoggedIn) {
      await router.push('/dashboard')
      toast({ ...messages.guestOnly, ...toastOptions(3, 'warning') })
    }
  }

  return <Component {...props} />
}

export default withGuest
