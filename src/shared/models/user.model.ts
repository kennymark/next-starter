export interface User {
  id: number
  email: string
  name: string
  confirmed: number
  created_at: string
  updated_at: string
}

export interface ResetPassword {
  password: string
  password_confirmation: string
}
