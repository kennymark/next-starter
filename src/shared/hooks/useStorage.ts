import { useState } from 'react'

type Res = [string, Function]

function useLocalStorage(key: string, initialValue?: string): Res {
  // State to store our value
  // Pass initial state function to useState so logic is only executed once
  const [storedValue, setStoredValue] = useState(() => {
    try {
      if (process.browser) {
        const item = globalThis.localStorage.getItem(key)
        return item ? item : initialValue
      }
    } catch (error) {
      return initialValue
    }
  })

  const setValue = (value) => {
    try {
      const valueToStore = value instanceof Function ? value(storedValue) : value
      setStoredValue(valueToStore)
      if (process.browser) {
        if (typeof value !== 'string') {
          globalThis.localStorage?.setItem(key, JSON.stringify(valueToStore))
        }
        globalThis.localStorage?.setItem(key, valueToStore)
      }
    } catch (error) {}
  }

  return [storedValue, setValue]
}

export default useLocalStorage
