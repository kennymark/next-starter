import { useStoreActions, useStoreState } from '@store/hooks'

function useAuth() {
  const { data, isLoggedIn, isSessionModalOpen, user } = useStoreState((state) => state.auth)
  const { setAuth, toggleSessionModal, signout, setUser } = useStoreActions((state) => state.auth)

  return {
    data,
    isLoggedIn,
    isSessionModalOpen,
    setAuth,
    toggleSessionModal,
    signout,
    user,
    setUser,
  }
}

export default useAuth
