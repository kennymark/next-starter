import { useEffect } from 'react'
import { useRouter } from 'next/router'
import { useToast } from '@chakra-ui/toast'
import { toastOptions } from '@shared/utils/toastOptions'

function useRequiredParamPage(param: string) {
  const { query, push } = useRouter()
  const toast = useToast()

  useEffect(() => {
    if (!query[param]) {
      push('/signin')
      toast({
        ...toastOptions(3, 'info'),
        title: 'Info',
        description: `This page does not contain the ${param} query string`,
      })
    }

    return () => {}
  }, [])
}

export default useRequiredParamPage
