module.exports = {
  rootDir: '../',
  setupFilesAfterEnv: ['<rootDir>/config/jest-setup.ts'],
  testPathIgnorePatterns: [
    "<rootDir>/.next/",
    "<rootDir>/node_modules/",
  ],
};
