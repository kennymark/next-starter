import { render, screen } from '@testing-library/react'
import React from 'react'

const add = (a: number, b: number) => a + b

describe('Initial tests', () => {
  test('add 2 numbers', () => {
    expect(add(2, 4)).toBe(6)
  })

  test('renders a div', async () => {
    render(<div>Wow</div>)
    expect(screen.getByText('Wow')).toContainHTML('<div>Wow</div>')
  })
})
