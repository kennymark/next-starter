import { Button, chakra, Heading, useToast } from '@chakra-ui/react'
import PasswordInput from '@shared/components/password-input'
import withGuest from '@shared/hoc/withGuest'
import { ResetPassword } from '@shared/models/user.model'
import authService from '@services/auth.service'
import { resetPasswordSchema } from '@shared/utils/schemas'
import { toastOptions } from '@shared/utils/toastOptions'
import { AxiosError } from 'axios'
import Layout from '@components/layout'
import { Form, FormikProvider, useFormik } from 'formik'
import { useRouter } from 'next/router'
import React from 'react'
import { useMutation } from 'react-query'

function ResetPasswoord() {
  const toast = useToast()
  const { asPath, push } = useRouter()

  const formik = useFormik({
    validateOnChange: true,
    validationSchema: resetPasswordSchema,
    initialValues: { password: '', password_confirmation: '' },
    onSubmit: (values) => mutate(values),
  })

  const { mutate } = useMutation(
    (values: ResetPassword) => authService.resetPassword(asPath, values),
    {
      onSuccess: ({ data }) => {
        toast({
          ...toastOptions(3, 'success'),
          title: 'Password changed',
          description: data.message,
        })
        setTimeout(() => push('/signin'), 1000)
      },
      onError: ({ response: { data } }: AxiosError) => {
        toast({
          ...toastOptions(3, 'error'),
          title: 'Password change failure',
          description: data?.message,
        })
      },
    }
  )

  return (
    <Layout title='Reset Password' sm>
      <Heading>Reset Password</Heading>

      <FormikProvider value={formik}>
        <chakra.form as={Form} mt={10}>
          <PasswordInput label='Password' type='password' name='password' />
          <PasswordInput label='Confirm Password' type='password' name='password_confirmation' />

          <Button variant='main' type='submit'>
            Reset Password
          </Button>
        </chakra.form>
      </FormikProvider>
    </Layout>
  )
}

export default withGuest(ResetPasswoord)
