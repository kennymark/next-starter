import { Button, chakra, FormControl, Heading, useToast } from '@chakra-ui/react'
import Layout from '@components/layout'
import authService from '@services/auth.service'
import PasswordInput from '@shared/components/password-input'
import TextInput from '@shared/components/text-input'
import withGuest from '@shared/hoc/withGuest'
import { isButtonEnabled } from '@shared/utils/form-utils'
import { signUpSchema } from '@shared/utils/schemas'
import { serverErrorToast } from '@shared/utils/server-errors'
import { Form, FormikProvider, useFormik } from 'formik'
import Router from 'next/router'
import React from 'react'
import { useMutation } from 'react-query'

function Invitation() {
  const toast = useToast()
  const formik = useFormik({
    validateOnChange: true,
    validationSchema: signUpSchema,
    initialValues: { password: '', email: '', name: '', password_confirmation: '' },
    onSubmit: (values) => mutate(values),
  })

  const { mutate } = useMutation(authService.signUp, {
    onSuccess: (_) => {
      Router.push(`signup/success?email=${formik.values.email}`)
    },
    onError: (err) => {
      serverErrorToast(toast, err, 'Invitation Failure')
    },
  })

  return (
    <Layout title='Sign up' sm>
      <Heading>Join team</Heading>

      <FormikProvider value={formik}>
        <chakra.form as={Form} mt={10}>
          <TextInput name='name' label='Name' />
          <TextInput type='email' name='email' label='Email' />
          <PasswordInput name='password' label='Password' />
          <PasswordInput name='password_confirmation' label='Confirm Password' />

          <FormControl>
            <Button variant='main' type='submit' isDisabled={isButtonEnabled(formik)}>
              Create Account
            </Button>
          </FormControl>
        </chakra.form>
      </FormikProvider>
    </Layout>
  )
}

export default withGuest(Invitation)
