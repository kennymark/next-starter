import { Box, Button, Flex, Heading, Text, useDisclosure } from '@chakra-ui/react'
import Layout from '@components/layout'
import ModalWithInput from '@components/modals/modal-with-input'
import withAuth from '@shared/hoc/withAuth'
import userService from '@services/user.service'
import React from 'react'
import { useQuery } from 'react-query'

function Team() {
  const { data } = useQuery('teams', userService.getTeam)
  const { isOpen, onOpen, onClose } = useDisclosure()
  const { isOpen: open, onOpen: openInvite, onClose: close } = useDisclosure()

  const createTeam = (values: ModelObject) => {
    onClose()
  }

  const inviteMembers = (emails: ModelObject) => {
    close()
  }

  return (
    <Layout title='Team'>
      <Flex justifyContent='space-between'>
        <Heading>Team</Heading>
        <Button variant='plain' onClick={onOpen}>
          Create Team
        </Button>
      </Flex>

      <Box>
        <Button my={10} onClick={openInvite}>
          Invite members
        </Button>
        <Text>{data?.data}</Text>
      </Box>

      <ModalWithInput
        inputName='team'
        yesText='Create Team'
        title='Create Team'
        description='Please enter the name of your team:'
        placeholder='Avengers'
        isOpen={isOpen}
        onClose={onClose}
        onYesClick={createTeam}
      />

      <ModalWithInput
        inputName='emails'
        yesText='Invite'
        size='2xl'
        title='Invite member'
        description='Please enter email(s) of new members'
        placeholder='johndoe@example.com'
        isOpen={open}
        onClose={close}
        onYesClick={inviteMembers}
      />
    </Layout>
  )
}

export default withAuth(Team)
