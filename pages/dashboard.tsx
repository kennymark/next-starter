import { Heading } from '@chakra-ui/react'
import Layout from '@components/layout'
import withAuth from '@shared/hoc/withAuth'
import React from 'react'

const Dashboard = withAuth(() => {
  return (
    <Layout title='Dashboard'>
      <Heading>Dashboard</Heading>
    </Layout>
  )
})

export default Dashboard
