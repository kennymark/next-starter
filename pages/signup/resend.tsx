import { Button, chakra, Container, Heading, Text, useToast } from '@chakra-ui/react'
import TextInput from '@shared/components/text-input'
import authService from '@services/auth.service'
import { resendEmailSchema } from '@shared/utils/schemas'
import { toastOptions } from '@shared/utils/toastOptions'
import SEO from '@components/seo'
import { Form, FormikProvider, useFormik } from 'formik'
import { useRouter } from 'next/router'
import React from 'react'
import { useMutation } from 'react-query'

function ResendConfirmationEmail() {
  const { query } = useRouter()
  const toast = useToast()

  const { mutate } = useMutation(authService.resendConfirmationEmail, {
    onSuccess: () => {
      toast({
        title: 'Email sent',
        description: 'Please check your inbox and confirm email',
        ...toastOptions(1, 'success'),
      })
    },
    onError: () => {
      toast({
        title: 'Email confirmation failure',
        description: 'There was an error sending email, please try again later',
        ...toastOptions(1, 'error'),
      })
    },
  })

  const formik = useFormik({
    validateOnChange: true,
    validationSchema: resendEmailSchema,
    initialValues: { email: query.email as string, confirm_email: 'string' },
    onSubmit: (values) => mutate(values.email),
  })

  return (
    <Container mt={100}>
      <SEO title='Resend Confirmation' />

      <Heading mb={5}>Resend confirmation email</Heading>

      <Text>
        If you did not recieve the email we sent you last time, you can request a new one using the
        same email or another one and the new confirmation email will be sent.
      </Text>

      <FormikProvider value={formik}>
        <chakra.form as={Form} mt={10}>
          <TextInput id='email' type='email' label='Email' />
          <TextInput id='confirm_email' type='email' label='Confirm Email' />

          <Button variant='main' type='submit'>
            Send email
          </Button>
        </chakra.form>
      </FormikProvider>
    </Container>
  )
}

export default ResendConfirmationEmail
