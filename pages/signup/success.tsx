import { Button, Container, Heading, List, ListItem, Text } from '@chakra-ui/react'
import BackArrow from '@components/back-arrow'
import SEO from '@components/seo'
import { useRouter } from 'next/router'
import React from 'react'

function SignupSuccess() {
  const router = useRouter()
  const {
    query: { email },
  } = router

  const resendEmail = () => {
    router.push(`/signup/resend?email=${email}`)
  }

  return (
    <Container mt={100}>
      <SEO title='Signup Success' />

      <Heading mb={5}>
        <BackArrow />
        Check your email
      </Heading>

      <Text bg='green.100' p={4} rounded='lg' color='green.800'>
        Congrats! We’ve sent a message to <strong>{email}</strong> with a link to activate your
        account.
      </Text>

      <Text fontWeight='bold' mt={5}>
        Did not recieve email?
      </Text>

      <Text my={2}>
        If you don’t see an email from us within a few minutes, a few things could have happened:
      </Text>

      <List styleType='disc' p={5}>
        <ListItem> The email is in your spam folder.</ListItem>
        <ListItem>The email address you entered had a mistake or typo.</ListItem>
        <ListItem>You accidentally gave us another email address.</ListItem>
        <ListItem>
          We can’t deliver the email to this address. (Usually because of corporate firewalls or
          filtering.)
        </ListItem>
      </List>

      <Button variant='main' type='submit' onClick={resendEmail}>
        Resend Email
      </Button>
    </Container>
  )
}

export default SignupSuccess
