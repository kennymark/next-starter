import { Button, Center, chakra, Flex, Heading, useToast } from '@chakra-ui/react'
import Layout from '@components/layout'
import NextLink from '@components/next-link'
import useAuth from '@hooks/useAuth'
import authService from '@services/auth.service'
import { getCurrentIP } from '@services/ip.service'
import oauthService from '@services/oauth.service'
import PasswordInput from '@shared/components/password-input'
import TextInput from '@shared/components/text-input'
import withGuest from '@shared/hoc/withGuest'
import { filterError, getRedirectUrl, isDev } from '@shared/utils/main'
import { signInSchema } from '@shared/utils/schemas'
import { toastOptions } from '@shared/utils/toastOptions'
import { AxiosError } from 'axios'
import { Form, FormikProvider, useFormik } from 'formik'
import { useRouter } from 'next/router'
import React, { useState } from 'react'
import { useMutation, useQuery } from 'react-query'

const { NEXT_PUBLIC_GITHUB_CLIENT_ID, NEXT_PUBLIC_GOOGLE_ID } = process?.env

const client_id = NEXT_PUBLIC_GITHUB_CLIENT_ID
const google_client_id = NEXT_PUBLIC_GOOGLE_ID

function Login() {
  const formik = useFormik({
    validateOnChange: true,
    validationSchema: signInSchema,
    initialValues: {
      email: isDev ? 'test@test.com' : '',
      password: '',
    },
    onSubmit: (values) => mutate(values),
  })

  const { setAuth } = useAuth()
  const router = useRouter()
  const toast = useToast()
  const [twitterToken, setTwitterToken] = useState('')
  const btn = { mt: 4, w: '100%', h: 50, as: 'a', cursor: 'pointer', _focus: {} } as any

  const { mutate } = useMutation(authService.signIn, {
    onSuccess: async ({ data }) => {
      setAuth(data)
      await router.push('/dashboard')
      toast({
        title: 'Login Successful.',
        description: data?.message,
        ...toastOptions(1, 'success'),
      })
    },
    onError: ({ response: { data } }: AxiosError) => {
      toast({
        description: filterError(data?.message),
        ...toastOptions(3, 'error'),
      })
    },
  })

  useQuery('ip', getCurrentIP)

  useQuery('twitter-auth', oauthService.fetchTwitterToken, {
    retry: 0,
    onSuccess: ({ data }) => {
      console.log(data?.data?.oauth_token)
      setTwitterToken(data?.data?.oauth_token)
    },
  })

  return (
    <Layout title='Sign in' sm>
      <Heading>Sign in</Heading>

      <FormikProvider value={formik}>
        <chakra.form as={Form} mt={10} autoComplete='false'>
          <TextInput type='email' name='email' label='Email' />
          <PasswordInput name='password' label='Password' />
          <Flex
            justify='space-between'
            fontSize='sm'
            cursor='pointer'
            pos='relative'
            bottom={2}
            display={{ base: 'block', md: 'flex' }}>
            <chakra.div>
              <NextLink to='/signup' fontWeight='bold'>
                Sign up for an account here
              </NextLink>
            </chakra.div>

            <chakra.div>
              <NextLink to='/forgot-password' color='gray.400'>
                Forgotten Password?
              </NextLink>
            </chakra.div>
          </Flex>

          <Button variant='main' type='submit'>
            Sign in
          </Button>

          <Center mt={4} color='gray.400' fontSize='xs' textTransform='uppercase'>
            or continue with
          </Center>

          <Flex>
            <Button
              {...btn}
              mr={2}
              variant='outline'
              href={`https://accounts.google.com/o/oauth2/auth/oauthchooseaccount?redirect_uri=${getRedirectUrl(
                'twitter'
              )}&response_type=permission%20id_token&scope=email%20profile%20openid&openid.realm&client_id=${google_client_id}&fetch_basic_profile=true&gsiwebsdk=2&flowName=GeneralOAuthFlow`}>
              <chakra.img src='/icons/google.svg' h={5} pr={4} />
              Google
            </Button>

            <Button
              variant='outline'
              colorScheme='twitter'
              {...btn}
              mr={2}
              href={`https://api.twitter.com/oauth/authenticate?oauth_token=${twitterToken}`}>
              <chakra.img src='/icons/twitter.svg' h={5} pr={4} />
              Twitter
            </Button>

            <Button
              variant='outline'
              borderColor='1px'
              {...btn}
              href={`https://github.com/login/oauth/authorize?scope=user&client_id=${client_id}&redirect_uri=${getRedirectUrl(
                'github'
              )}`}>
              <chakra.img src='/icons/github.svg' h={5} pr={4} />
              Github
            </Button>
          </Flex>
        </chakra.form>
      </FormikProvider>
    </Layout>
  )
}

export default withGuest(Login)
