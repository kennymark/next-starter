import { chakra } from '@chakra-ui/react'
import Cta from '@components/cta'
import Layout from '@components/layout'
import authService from '@services/auth.service'
import { useQuery } from 'react-query'

function IndexPage() {
  useQuery('test-request', authService.test)

  return (
    <Layout title='Home'>
      <Cta />
      {/* <chakra.img src='team-of-critters.svg' maxW='full' alt='Four one-eyed aliens playing' /> */}
    </Layout>
  )
}

export default IndexPage
