import { Alert, Button, chakra, Heading, useToast } from '@chakra-ui/react'
import Layout from '@components/layout'
import TextInput from '@shared/components/text-input'
import messages from '@shared/data/toast-messages'
import withGuest from '@shared/hoc/withGuest'
import authService from '@services/auth.service'
import { isDev } from '@shared/utils/main'
import { forgotPasswordSchema } from '@shared/utils/schemas'
import { toastOptions } from '@shared/utils/toastOptions'
import { Form, FormikProvider, useFormik } from 'formik'
import Link from 'next/link'
import React, { useState } from 'react'
import { useMutation } from 'react-query'

function ForgotPassword() {
  const toast = useToast()
  const [url, setUrl] = useState('')

  const formik = useFormik({
    validateOnChange: true,
    validationSchema: forgotPasswordSchema,
    initialValues: { email: '' },
    onSubmit: (values) => mutate(values),
  })

  const { mutate } = useMutation(authService.forgotPassword, {
    onSuccess: ({ data }) => {
      toast({ ...toastOptions(), ...messages.forgotPassword, status: 'success' })
      setUrl(data?.link)
    },
    onError: ({ response: { data } }) => {
      toast({ ...toastOptions(3, 'error'), description: data.message, title: 'Fail' })
    },
  })

  return (
    <Layout title='Forgot Password' sm>
      <Heading>Enter email</Heading>

      {url && isDev && (
        <Alert maxW={600} p={3} overflowX='scroll' rounded='lg'>
          <Link href={url}>
            <a>{url}</a>
          </Link>
        </Alert>
      )}

      <FormikProvider value={formik}>
        <chakra.form as={Form} mt={10}>
          <TextInput type='email' name='email' label='Email' />

          <Button variant='main' type='submit'>
            Submit
          </Button>
        </chakra.form>
      </FormikProvider>
    </Layout>
  )
}

export default withGuest(ForgotPassword)
