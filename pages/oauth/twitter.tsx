import { Heading, useToast } from '@chakra-ui/react'
import Layout from '@components/layout'
import oauthService from '@services/oauth.service'
import { toastOptions } from '@shared/utils/toastOptions'
import { AxiosError } from 'axios'
import { useRouter } from 'next/router'
import React from 'react'
import { useQuery } from 'react-query'

function TwitterCallback() {
  const { query } = useRouter()
  const toast = useToast()
  const router = useRouter()

  const { data } = useQuery('twitter', () => oauthService.sendTokenAndVerifier(query), {
    enabled: !!query.oauth_token,
    onSuccess: ({ data }) => {
      console.log(data)
      toast({ ...toastOptions(3, 'success'), description: data.message })
    },

    onError: (error: AxiosError) => {
      toast({ ...toastOptions(3, 'error'), description: error.response?.data.message! })
    },
  })

  return (
    <Layout>
      <Heading mb={4}>Twitter Oauth Callback</Heading>
      <pre>{JSON.stringify(data?.data.data, null, 2)}</pre>
    </Layout>
  )
}

export default TwitterCallback
