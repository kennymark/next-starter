import { Heading, useToast } from '@chakra-ui/react'
import Layout from '@components/layout'
import useRequiredParamPage from '@hooks/useRequiredParamPage'
import oauthService from '@services/oauth.service'
import { toastOptions } from '@shared/utils/toastOptions'
import { AxiosError } from 'axios'
import { useRouter } from 'next/router'
import React from 'react'
import { useQuery } from 'react-query'

function GithubCallback() {
  const { query } = useRouter()
  const toast = useToast()
  const router = useRouter()
  // useRequiredParamPage('code')

  const { data } = useQuery(
    'github-code',
    () => oauthService.sendGithubCode(query.code as string),
    {
      enabled: !!query.code,

      onSuccess: ({ data }) => {
        toast({ ...toastOptions(3, 'success'), description: data.message })
        //TODO - Signup user up, log them in
      },

      onError: ({ response }: AxiosError) => {
        toast({ ...toastOptions(3, 'error'), description: response?.data.message! })
      },
    }
  )

  return (
    <Layout>
      <Heading mb={4}>Github Oauth Callback</Heading>
      <pre>{JSON.stringify(data?.data.user, null, 2)}</pre>
    </Layout>
  )
}

export default GithubCallback
