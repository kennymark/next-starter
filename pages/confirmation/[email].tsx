import {
  Alert,
  AlertDescription,
  AlertIcon,
  AlertProps,
  AlertStatus,
  AlertTitle,
  Button,
  Container,
  FormControl,
  useToast,
} from '@chakra-ui/react'
import SEO from '@components/seo'
import withGuest from '@shared/hoc/withGuest'
import authService from '@services/auth.service'
import { toastOptions } from '@shared/utils/toastOptions'
import { useRouter } from 'next/router'
import React, { useState } from 'react'
import { useMutation, useQuery } from 'react-query'
import RenderIf from '@components/render-if'

function ConfirmEmail() {
  const {
    query: { email, signature },
    push,
  } = useRouter()
  const { sendConfirmationEmail, resendConfirmationEmail } = authService
  const [accountConfirmed, setAccountConfirmed] = useState(true)
  const [message, setMessage] = useState('')
  const toast = useToast()

  useQuery('confirm', () => sendConfirmationEmail(String(email), String(signature)), {
    enabled: !!email,
    onSuccess: ({ data }) => {
      setAccountConfirmed(true)
      setMessage(data?.message)
    },
    onError: ({ response: { data } }) => {
      setAccountConfirmed(false)
      setMessage(data?.message)
    },
  })

  const { mutate } = useMutation(resendConfirmationEmail, {
    onSuccess: (_) => {
      toast({ ...toastOptions(), description: 'Email Sent' })
    },
    onError: (_) => {
      toast({ ...toastOptions(), title: 'Email Not Sent', status: 'error' })
    },
  })

  return (
    <Container pt={20}>
      <SEO title={`Email confirmation for ${email}`} />

      <RenderIf
        condition={accountConfirmed}
        elseShow={
          <Alerta message={message} status='error' onClick={() => mutate(String(email))} />
        }>
        <Alerta message={message} status='success' />
      </RenderIf>

      <FormControl>
        <Button variant='main' onClick={() => push('/signin')}>
          Go back to Signin page
        </Button>
      </FormControl>
    </Container>
  )
}

interface AlertaProps {
  message: string
  status: AlertStatus
  onClick?: () => void
}

function Alerta({ message, status, onClick }: AlertaProps) {
  const alertProps = {
    variant: 'subtle',
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'center',
    height: '200px',
    rounded: 'lg',
  } as AlertProps

  if (status === 'error') {
    return (
      <Alert status='error' {...alertProps}>
        <AlertIcon boxSize='40px' mr={0} />

        <AlertTitle mt={4} mb={1} fontSize='lg'>
          Account Confirmation Error
        </AlertTitle>

        <AlertDescription maxWidth='sm'>{message}</AlertDescription>

        <Button my={2} variant='ghost' _hover={{ bg: 'red.200' }} color='red.900' onClick={onClick}>
          Resend Email
        </Button>
      </Alert>
    )
  }

  return (
    <Alert status='success' {...alertProps}>
      <AlertIcon boxSize='40px' mr={0} />

      <AlertTitle mt={4} mb={1} fontSize='lg'>
        Account Confirmed
      </AlertTitle>

      <AlertDescription maxWidth='sm'>{message}</AlertDescription>
    </Alert>
  )
}

export default withGuest(ConfirmEmail)
