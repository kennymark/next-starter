import { Button, chakra, Heading, Text, useToast } from '@chakra-ui/react'
import UpdatePassword from '@components/account/update-password'
import Layout from '@components/layout'
import DeleteUserModal from '@components/modals/delete-user'
import useAuth from '@hooks/useAuth'
import userService from '@services/user.service'
import { isButtonEnabled } from '@shared/utils/form-utils'
import SimpleInput from '@shared/components/simple-input'
import TextInput from '@shared/components/text-input'
import messages from '@shared/data/toast-messages'
import withAuth from '@shared/hoc/withAuth'
import { accountSchema } from '@shared/utils/schemas'
import { toastOptions } from '@shared/utils/toastOptions'
import time from 'dayjs'
import localizedFormat from 'dayjs/plugin/localizedFormat'
import { Form, FormikProvider, useFormik } from 'formik'
import React from 'react'
import { useMutation } from 'react-query'

time.extend(localizedFormat)

function Account({ user: cachedUser }) {
  const formatString = 'ddd, MMMM DD YYYY HH:mm'
  const { user, setUser } = useAuth()
  const toast = useToast()

  const formik = useFormik({
    validateOnChange: true,
    validationSchema: accountSchema,
    initialValues: user,
    onSubmit: (values) => {
      const newData = { oldEmail: user?.email, ...values }
      mutate(newData)
    },
  })

  const { mutate } = useMutation(userService.updateUser, {
    onMutate: (vars) => setUser({ ...vars, updated_at: new Date().toISOString() }),
    onSuccess: ({ data: res }) => {
      toast({
        ...messages.accountUpdated,
        description: res?.message,
        ...toastOptions(3, 'success'),
      })
    },
    onError: (error) => {
      toast({
        ...messages.defaultError,
        description: error.toString(),
        ...toastOptions(3, 'error'),
      })
      setUser(cachedUser)
    },
  })

  return (
    <Layout title='My Account'>
      <Heading>My Account</Heading>

      <FormikProvider value={formik}>
        <chakra.form as={Form} mt={50}>
          <TextInput name='name' />
          <TextInput name='email' />

          <SimpleInput
            label='Created on'
            isDisabled
            defaultValue={time(user?.created_at).format(formatString)}
          />

          <SimpleInput
            label='Last updated'
            isDisabled
            defaultValue={time(user?.updated_at).format(formatString)}
          />

          <Button variant='main' type='submit' w={60} isDisabled={isButtonEnabled(formik)}>
            Update
          </Button>
        </chakra.form>
      </FormikProvider>

      <UpdatePassword email={user?.email} />

      <Heading fontSize='lg' mt={20}>
        Delete your account
      </Heading>
      <Text>Your account will be permanently deleted</Text>

      <DeleteUserModal id={user?.id} />
    </Layout>
  )
}

export default withAuth(Account)
