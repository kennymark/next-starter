import { ChakraProvider } from '@chakra-ui/react'
import SessionExpiredModal from '@components/modals/session-expired-modal'
import store from '@store/index'
import theme from '@shared/utils/theme'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'
import 'src/css/index.css'
import 'src/css/nprogress.css'

export const queryClient = new QueryClient({
  defaultOptions: { queries: { retry: 2 } },
})

const MyApp = ({ Component, pageProps }) => {
  return (
    <StoreProvider store={store}>
      <QueryClientProvider client={queryClient}>
        <ChakraProvider theme={theme}>
          <Component {...pageProps} />
          <SessionExpiredModal />

          {/* <AllowCookies /> */}
        </ChakraProvider>

        <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
    </StoreProvider>
  )
}

export default MyApp
